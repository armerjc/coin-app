//
//  ViewController.swift
//  CoinApp
//
//  Created by Abhay Singh on 09/04/15.
//  Copyright (c) 2015 Aryan Katwal. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import iAd

class ViewController: UIViewController, AVAudioPlayerDelegate {

   // var clickCount = 0
    var player : AVAudioPlayer! = nil
    @IBOutlet var coinImageView: UIImageView!
    @IBOutlet var btnFlipImage: UIButton!

    // Re-add iAds
    override func viewDidLoad() {
        
    canDisplayBannerAds = true
        
    }

    // Re-add delay function
    func delay(delay:Double, closure:()->()) {
        
        dispatch_after(
            dispatch_time( DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
    }

    // Button
    @IBAction func btnFlipImageClicked(sender: AnyObject) {
        
        // sets the path for the sound file to play
        let path = NSBundle.mainBundle().pathForResource("coinFlip", ofType:"caf")
        
        // sets the path to be a file?
        let fileURL = NSURL(fileURLWithPath: path!)
        
        do{
        // tells the audio player to from a file
        player = try AVAudioPlayer(contentsOfURL: fileURL, fileTypeHint: nil)
        }
        catch{
            print(error)
            
        }
        // gets ready to play
        player.prepareToPlay()
        
        // tells the app that it is in charge of playing the sound
        player.delegate = self
        
        // play the sound!
        player.play()
        
       
        self.coinImageView.image = UIImage(named:"2")
    
        delay(0.2) {
        return self.randomImage()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        // Re-enable the App to have random results -- not sure why this was changed from my original working project?!
    func randomImage()
    {
        
        let image = Int(arc4random_uniform(2))
        
        print("nothing")
        
        delay(0.5){
            
            switch (image) {
                
                // first case
            case 0:
                self.coinImageView.image = UIImage(named:"1")
                break;
                
                // second case
            case 1:
                self.coinImageView.image = UIImage(named:"0")
                
            default:
                break;
            }
        }
    }
    
    func showRendomImage()
    {
        let image = random() % 2;
        let imageName = NSString(format:"d.png", image)
        coinImageView.image = UIImage(named: imageName as String)
    }
    
}

