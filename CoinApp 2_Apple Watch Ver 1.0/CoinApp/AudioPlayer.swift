//
//  AudioPlayer.swift
//  iTextSpeed2
//
//  Created by Joshua Armer on 7/23/15.
//  Copyright (c) 2015 Joshua Armer. All rights reserved.
//

import Foundation
import AVFoundation
//var nineQuintillionTwoHundredTwentyThreeQuadrillionThreeHundredSeventyTwoTrillionThirtySixBillionEightHundredFiftyFourMillionSevenHundredSeventyFiveThousandEightHundredSeven =9223372036854775807

var soundPlayer = AVAudioPlayer()

func playSound() {
    
    // Sets up our audiofile path
    let audioPath = NSBundle.mainBundle().pathForResource("sent", ofType: "caf")!
    
    do {
        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
    } catch _ {
    }
    do {
        try AVAudioSession.sharedInstance().setActive(true)
    } catch _ {
    }
    
    
    // Error variable.
    //var error: NSError? = nil
    do {
        // This sets up our error trap.
        
        soundPlayer = try AVAudioPlayer(contentsOfURL: NSURL(string: audioPath)!)
    } catch {
    
    //var error1 as NSError {
       // error = error1
        //soundPlayer = nil
    }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
}

    