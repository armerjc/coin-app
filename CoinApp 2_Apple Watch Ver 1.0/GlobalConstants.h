//
//  GlobalConstants.h
//  SOTI hub
//
//  Created by Abhay Singh on 17/03/15.
//  Copyright (c) 2015 Abhay Singh. All rights reserved.
//

#ifndef SOTI_hub_GlobalConstants_h
#define SOTI_hub_GlobalConstants_h



// MARK:-   NSUserDefaults Contstants
#define KIsLogedIn     "sLogedIn"


// MARK:-   Notiifcations Constants
#define KapiCallBack     "apiCallBack"



#define KserverBaseUrl   "https://webdev3.inqa.soti.net/"


#define KlocalDbName "SOTIhubDatabase"



#define KFilterScreenTittle "Filters"
#define KSortingScreenTittle "Sorting"
#define KSearchScreenTittle "Search"
#define KSettingScreenTittle "Settings"
#define KFilterScreenTittle "Filters"




#endif
