//
//  InterfaceController.swift
//  CoinAppWatchKit Extension
//
//  Created by Joshua Armer on 10/24/15.
//  Copyright © 2015 Aryan Katwal. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet var coinImage: WKInterfaceButton!
    
    @IBAction func flipCoinButton() {
        
        WKInterfaceDevice.currentDevice().playHaptic(.Click)
        
        self.coinImage.setBackgroundImageNamed("2")
        
        delay(0.2) {
            return self.randomImage()
        }
        
    }

    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    // Re-add delay function
    func delay(delay:Double, closure:()->()) {
        
        dispatch_after(
            dispatch_time( DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
    }
    
    func randomImage()
    {
        
        let image = Int(arc4random_uniform(2))
        
        print("nothing")
        
        delay(0.5){
            
            switch (image) {
                
                // first case
            case 0:
        self.coinImage.setBackgroundImageNamed("1")
                WKInterfaceDevice.currentDevice().playHaptic(.Success)
                break;
                
                // second case
            case 1:
        self.coinImage.setBackgroundImageNamed("0")
                WKInterfaceDevice.currentDevice().playHaptic(.Success)
                
            default:
                break;
            }
        }
    }
    
    func showRendomImage()
    {
        let image = random() % 2;
        let imageName = NSString(format:"d.png", image)
        self.coinImage.setBackgroundImageNamed(imageName as String)
    }
    
}
